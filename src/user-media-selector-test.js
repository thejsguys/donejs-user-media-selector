import QUnit from 'steal-qunit/steal-qunit'
import { ViewModel } from './user-media-selector'

QUnit.module('user-media-selector')

QUnit.test('vm can be instanceof ViewModel', () => {
  const vm = new ViewModel()
  QUnit.ok(vm instanceof ViewModel)
})

QUnit.test('constraints getter returns valid constrains object', () => {
  const vm = new ViewModel({
    selectedVideoDevice: 'default',
    selectedAudioDevice: 'default',
    height: 720,
    width: 1280
  })

  QUnit.deepEqual(vm.constraints, {
    video: {
      deviceId: {
        exact: 'default'
      },
      height: 720,
      width: 1280
    },
    audio: {
      deviceId: {
        exact: 'default'
      }
    }
  })

  vm.selectedAudioDevice = ''

  QUnit.deepEqual(vm.constraints, {
    video: {
      deviceId: {
        exact: 'default'
      },
      height: 720,
      width: 1280
    },
    audio: false
  })

  vm.selectedVideoDevice = ''
  vm.selectedAudioDevice = 'default'

  QUnit.deepEqual(vm.constraints, {
    video: false,
    audio: {
      deviceId: {
        exact: 'default'
      }
    }
  })
})

QUnit.test('videoDevices and audioDevices return filter list', () => {
  const vm = new ViewModel({
    devices: [
      {
        kind: 'videoinput',
        deviceId: 'first'
      },
      {
        kind: 'videoinput',
        deviceId: 'second'
      },
      {
        kind: 'audioinput',
        deviceId: 'first'
      },
      {
        kind: 'audioinput',
        deviceId: 'second'
      }
    ]
  })

  QUnit.equal(vm.videoDevices.length, 2)
  QUnit.equal(vm.audioDevices.length, 2)
  QUnit.equal(vm.selectedVideoDevice, 'first')
  QUnit.equal(vm.selectedAudioDevice, 'first')
})
