# donejs-user-media-selector

A user media selector with a live preview

![Demo image](demo-image.png)

## Usage

### ES6 use

With StealJS, you can import this module directly in a template that is autorendered:

```js
import plugin from 'donejs-user-media-selector';
```

### CommonJS use

Use `require` to load `donejs-user-media-selector` and everything else
needed to create a template that uses `donejs-user-media-selector`:

```js
var plugin = require("donejs-user-media-selector");
```

### Standalone use

Load the `global` version of the plugin:

```html
<script src='./node_modules/donejs-user-media-selector/dist/global/donejs-user-media-selector.js'></script>
```

### Example

```html
  <can-import from="user-media-selector"/>
  <user-media-selector
    height:raw="720"
    width:raw="1280"
    constraints:to="constraints"
  />
```

### Properties

#### constraints
A read-only and provides a [media constraints](https://developer.mozilla.org/en-US/docs/Web/API/MediaStreamConstraints) object to the consumer.

#### height
A writable, optional property that allows the consumer to provide a height used for the video in the constraints object

#### width
A writable, optional property that allows the consumer to provide a width used for the video in the constraints object
